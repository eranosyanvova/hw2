package com.example.hw2.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/hi")
    public String sayHi() {
        return "Привет";
    }

    @GetMapping("/bye")
    public String sayBye() {
        return "Пока.";
    }
}
